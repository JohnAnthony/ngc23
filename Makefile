.phony: tidy

all: one.pdf two.pdf combined.pdf tidy

tidy:
	rm -f build/*.{log,aux,toc}

one.pdf: one.tex img/*
	pdflatex --halt-on-error "one.tex"

two.pdf: two.tex img/*
	pdflatex --halt-on-error "two.tex"

combined.pdf: one.pdf two.pdf
	gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dAutoRotatePages=/None \
		-sOutputFile=combined.pdf one.pdf two.pdf
